var fs = require("fs");
var fnapi = require("./fnapi");

let step = 1;

const getResults = (sid, count, delay, save) => {
  setTimeout(_ => {
    fnapi.handleResponse(fnapi.searchResults(sid), response => {

      const results = response.OTA_AirLowFareSearchRS;
      const itineraries = results && results.PricedItineraries || [];
      const complete = "Completed" == results.Status || count <= 0;

      console.log(`results step=${step} status=${results.Status} count=${itineraries.length}`);

      if (save)
        fs.writeFileSync(`search-${step++}.json`, JSON.stringify(response, null, 2));

      if (complete) {
        if (itineraries.length) {

          for (let fare of findDistinct(10, "airlines", itineraries))
            console.log(`${fare.cid}\t${fare.price}\t${fare.vendor}\t${fare.airlines}`);

          console.log(`?qid=${sid}&cid=${itineraries[0].AirItineraryPricingInfo.QuoteID}`);
          console.log(itineraries[0].AirItineraryEncode.enc);

        } else {
          console.log("No flights found");
        }

      } else {
        getResults(sid, count - 1, delay, save);
      }
    })
  }, delay);
};

const findDistinct = (count, key, itineraries) => {
  const fares = itineraries.map(itinerary => ({
    cid: itinerary.AirItineraryPricingInfo.QuoteID,
    vendor: itinerary.AirItineraryPricingInfo.FareInfos.FareInfo.TPA_Extensions.ProviderCode,
    airlines: mapAirlines(itinerary.AirItinerary.OriginDestinationOptions.OriginDestinationOption),
    price: itinerary.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount,
  }));
  const distinct = fares.reduce((acc, fare) => {
    if (!acc[fare[key]] && Object.keys(acc).length < count)
      acc[fare[key]] = fare;
    return acc;
  }, {});
  return Object.keys(distinct).map(key => distinct[key]);
};

const mapAirlines = legs =>
  legs.map(leg =>
    leg.FlightSegment.map(segment =>
      segment.MarketingAirline.Code
    ).join("/")
  ).join("/");

module.exports = args => {
  const [
    actionName,
    trip,
    passengers,
    tripClass,
    clientRef,
    currency,
  ] = args._;

  const request = fnapi.parseRequest(
    trip,
    tripClass,
    passengers,
    clientRef,
    currency,
  );

  console.log("searching", fnapi.toQueryString(request));

  fnapi.handleResponse(fnapi.searchAsync(request), response => {

    let results = response.OTA_AirLowFareSearchRS;
    let sid = results.EchoToken;

    console.log("session", sid);

    getResults(sid, 30, 1000, args["save-steps"]);
  });
}
