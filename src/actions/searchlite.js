const fs = require("fs");
const pointer = require("json-ptr");
const fnapi = require("./fnapi");
const validator = require("../schema/validators").search;

const PollCountMax = 30;
const PollDelaySeconds = 1;

const fixResponse = response => ({
  ...response,
  airlines: response.airlines.reduce((airlines, airline) => ({
    ...airlines,
    [airline.id]: airline,
  }), {}),
  airports: response.locations.reduce((airports, airport) => ({
    ...airports,
    [airport.id]: {
      ...airport,
      state: "",
    },
  }), {}),
  locations: null,
  itineraries: response.itineraries.map(itinerary => ({
    ...itinerary,
    legs: itinerary.legs.map(leg => ({
      ...leg,
      segments: leg.segments.map(segment => ({
        ...segment,
        origin: `#/airports/${segment.origin}`,
        destination: `#/airports/${segment.destination}`,
        airline: `#/airlines/${segment.airline.marketing}`,
        ...(segment.airline.operating ? { operatingAirline: `#/airlines/${segment.airline.operating}` } : {}),
        arrival: segment.arrival + "-00:00",
        departure: segment.departure + "-00:00",
      }))
    }))
  }))
});

const mapItineraryPointers = response =>
  response.itineraries
    .map(itinerary => ({
      ...itinerary,
      legs: itinerary.legs.map(leg => ({
        ...leg,
        segments: leg.segments.map(segment => ({
          ...segment,
          origin: pointer.create(segment.origin).get(response),
          destination: pointer.create(segment.destination).get(response),
          airline: pointer.create(segment.airline).get(response),
          ...(segment.operatingAirline ? { operatingAirline: pointer.create(segment.operatingAirline).get(response) } : {}),
        }))
      }))
    }));

let lastItineraryId;
const findLastItineraryId = itineraries =>
  lastItineraryId = itineraries
    .reduce((lastId, itinerary) =>
      Math.max(
        lastId,
        itinerary.id.match(/(\d+)/g)[0]
      )
    , lastItineraryId);

const getResults = (sid, cid, count, delay, callback) => {
  setTimeout(_ => {
    fnapi.searchliteResponse(fnapi.searchliteResults(sid, cid), response => {
      // HACK!
      response = fixResponse(response);

      if (!validator.validateResponse(response))
        console.log("response failed validation", validator.errors);

      const done =
        "Completed" == response.status ||
        count <= 0;
      
      findLastItineraryId(response.itineraries || []);

      if (callback)
        callback(response, done);

      if (!done)
        getResults(sid, lastItineraryId, count - 1, delay, callback);
    })
  }, delay);
};

module.exports = args => {
  const [
    actionName,
    trip,
    passengers,
    tripClass,
    clientRef,
    currency,
  ] = args._;

  const request = fnapi.parseRequest(
    trip,
    tripClass,
    passengers,
    clientRef,
    currency,
  );

  console.log("searching", fnapi.toQueryString(request));

  fnapi.searchliteResponse(fnapi.searchliteAsync(request), response => {

    const sid = response.id;
    console.log("session", sid);

    const data = {
      itineraries: [],
      airports: {},
      airlines: {},
    };

    let step = 1;
    lastItineraryId = 0;

    getResults(sid, null, PollCountMax, PollDelaySeconds * 1000, (response, done) => {

      data.itineraries.push(...(response.itineraries || []));
      Object.assign(data.airports, response.airports);
      Object.assign(data.airlines, response.airlines);

      let count = data.itineraries.length;
      data.itineraries = data.itineraries
        .filter(itinerary =>
          !response.removedItineraries.includes(itinerary.id)
        );
      const removed = count - data.itineraries.length;
      count = data.itineraries.length;

      if (args["save-steps"])
        fs.writeFileSync(`searchlite-${step}.json`, JSON.stringify(response, null, 2));

      console.log(`response step=${step++} status=${response.status} progress=${response.progress} last=${lastItineraryId} new=${response.itineraries.length} removed=${removed} count=${count}`);

      if (done) {
        if (count) {

          if (args["map-pointers"])
            data.itineraries = mapItineraryPointers(data);

          const dupes = Math.max(
            ...Object.values(
              data.itineraries
                .reduce((ids, i) => {
                  ids[i.id] = (ids[i.id] || 0) + 1;
                  return ids;
                }, {})
            )
          );
          if (dupes > 1)
            console.log("DUPES!!!", dupes);

          const itinerary = data.itineraries[0];
          console.log(`?qid=${response.id}&cid=${itinerary.id}`);
          console.log(itinerary.enc);

          if (args["save-data"])
            fs.writeFileSync("searchlite.json", JSON.stringify(data, null, 2));

        } else {
          console.log("No flights found");
        }
      }

    });
  });
}
