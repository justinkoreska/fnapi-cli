
module.exports = {
  search: require("./search.js"),
  review: require("./review.js"),
  flex: require("./flex.js"),
  searchlite: require("./searchlite.js"),
};
