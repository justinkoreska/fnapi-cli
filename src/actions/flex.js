var fnapi = require("./fnapi");

module.exports = args => {
  const [
    actionName,
    sid,
  ] = args;

  console.log(`getting flex data for ${sid}`);

  fnapi.handleResponse(fnapi.flexdata(sid), response => {
    console.log(JSON.stringify(response, null, 2));
  });
};
