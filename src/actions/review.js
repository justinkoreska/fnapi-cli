var fnapi = require("./fnapi");

module.exports = args => {
  const [
    actionName,
    sid,
    cid,
  ] = args._;

  console.log(`reviewing ${sid}/${cid}`);

  const request = {
    sid,
    cid,
    //lang: "EN",
    //currency: "CAD",
    ancillary_options: 1,
  };

  fnapi.handleResponse(fnapi.review(request), response => {
    console.log(JSON.stringify(response, null, 2));
  });
};
