const fetch = require("node-fetch");
const moment = require("moment");

const apiBase = process.env.FNAPI_BASE || "https://fnxml.flightnetwork.com/api";

const apiSearch = "/search/json";
const apiSearchAsync = "/search/async/json";
const apiSearchResults = "/search/results/json";
const apiReview = "/review/json";
const apiFlexdata = "/flexdata";

const parseRequest = (trip, tripClass, passengers, clientRef, currency) => {
  const legs = trip
    .split(",")
    .map(leg => {
      const [ from, to, dateString ] = leg.split(".");
      const date = moment(dateString).format("YYYY-MM-DD");
      return { from, to, date };
    });
  const [ adult, child, infant ] = (passengers || "1,0,0").split(",");
  const request = {
    trip_class: tripClass || "Economy",
    client_ref: clientRef || "flightnetwork",
    currency: currency || "CAD",
    Adult: adult || 1,
    Child: child || 0,
    Infant: infant || 0,
    //flex: 1,
    //nonstop: 1,
    //source: "",
    //show_fare_family: 1,
    //show_source_price: 1,
    //max_results: 0,
    //lang: "en",
    //client_ip: "",
    //locale: "",
    //PDS: "",
    //appVersion: "",
    //uuid: "",
    //referrer: "",
    //cmpid: "",
    //agent_id: "",
    //getCachedSearch: "",
  };
  if (1 == legs.length)
    Object.assign(request, {
      trip_type: "Oneway",
      dep_from: legs[0].from,
      dep_to: legs[0].to,
      departure_date: legs[0].date,
    });
  if (2 == legs.length)
    Object.assign(request, {
      trip_type: "Roundtrip",
      dep_from: legs[0].from,
      dep_to: legs[0].to,
      departure_date: legs[0].date,
      ret_from: legs[1].from,
      ret_to: legs[1].to,
      return_date: legs[1].date,
    });
  if (2 < legs.length)
    Object.assign(request, {
      trip_type: "Multileg",
      ...legs.reduce((legs, leg, index) => ({
        ...legs,
        [`dep_from${index+1}`]: leg.from,
        [`dep_to${index+1}`]: leg.to,
        [`departure_date${index+1}`]: leg.date,
      }), {})
    });
  
  return request;
};

const toQueryString = obj => {
  var parts = [];
  for (var i in obj)
    if (obj.hasOwnProperty(i))
      parts.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
  return parts.join("&");
};

const makeFormPost = form => ({
  method: "POST",
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
  },
  body: toQueryString(form),
});

const handleResponse = (promise, callback) =>
  promise
    .then(response => response
      .json()
      .then(json => {
        const errors =
          json.OTA_AirLowFareSearchRS && json.OTA_AirLowFareSearchRS.Errors ||
          json.OTA_AirPriceRS && json.OTA_AirPriceRS.Errors;
        if (errors)
          return Promise.reject(errors);
        if (callback)
          callback(json);
      })
    )
    .catch(console.log);

const search = request =>
  fetch(apiBase + apiSearch, makeFormPost(request));

const searchAsync = request =>
  fetch(apiBase + apiSearchAsync + "?" + toQueryString(request));

const searchResults = sid =>
  fetch(apiBase + apiSearchResults, makeFormPost({ sid }));

const review = request =>
  fetch(apiBase + apiReview, makeFormPost(request));

const flexdata = sid =>
  fetch(apiBase + apiFlexdata + "/" + sid);


const searchliteResponse = (promise, callback) =>
  promise
    .then(response => response
      .json()
      .then(json => {
        if (json.code)
          return Promise.reject(json);
        if (callback)
          callback(json);
      })
    )
    .catch(console.log);

const searchliteAsync = request =>
  fetch(apiBase + apiSearchAsync + "?" + toQueryString(request), {
    headers: {
      "Accept": "searchlite",
    }
  });

const searchliteResults = (sid, lastCid) =>
  fetch(apiBase + apiSearchResults, {
    method: "POST",
    headers: {
      "Accept": "searchlite",
      "Content-Type": "application/x-www-form-urlencoded",
    },
    body: toQueryString({ sid, last_cid: lastCid }),
  });

module.exports = {
  toQueryString,
  parseRequest,
  handleResponse,
  search,
  searchAsync,
  searchResults,
  review,
  flexdata,
  searchliteAsync,
  searchliteResults,
  searchliteResponse,
};
