#!/usr/bin/env node

const args = require("minimist")(process.argv.slice(2));
const actions = require("./actions");

const actionName = args._[0];
const action = actions[actionName];

if (!action) {
  console.log("Unknown action", actionName);
  process.exit(1);
}

action(args);
