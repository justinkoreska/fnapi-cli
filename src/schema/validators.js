const Ajv = require("ajv");

const searchSchema = require("./search.schema.json");

class SearchValidator {

  constructor() {
    this._validator =
      new Ajv()
        .addSchema(searchSchema, "search");

    if (this._validator.errors)
      throw new Error(this._validator.errors);
  }

  validateResponse(response) {
    return this
      ._validator
      .validate(
        "search#/definitions/response",
        response
      );
  }

  get errors() {
    return this._validator.errors;
  }

}

module.exports = {
  search: new SearchValidator(),
};
