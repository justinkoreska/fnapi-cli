const fs = require("fs");
const validators = require("./validators");

const response = JSON.parse(fs.readFileSync("/dev/stdin", "utf8"));
const isValid = validators.search.validateResponse(response);

if (isValid)
  console.log("ok");
else
  console.log("fail", JSON.stringify(validators.search.errors, null, 2));
