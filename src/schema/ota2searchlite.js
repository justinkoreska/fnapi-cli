const fs = require("fs");

const ota = JSON.parse(fs.readFileSync("/dev/stdin", "utf8")).OTA_AirLowFareSearchRS;

const mapResult = result => ({
  id: `${result.SequenceNumber}`,
  enc: result.AirItineraryEncode.enc,
  priority: result.AirItineraryPricingInfo.TPA_Extensions.PriorityScore,
  fare: {
    base: result.AirItineraryPricingInfo.ItinTotalFare.BaseFare.Amount,
    fees:
      result.AirItineraryPricingInfo.ItinTotalFare.Taxes.Tax.Amount +
      result.AirItineraryPricingInfo.ItinTotalFare.Fees.Fee.reduce((total, fee) => total + fee.Amount, 0), //?
    total: result.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount,
  },
  legs: result.AirItinerary.OriginDestinationOptions.OriginDestinationOption.map(leg => ({
    segments: leg.FlightSegment.map(segment => ({
      provider: result.AirItineraryPricingInfo.FareInfos.FareInfo.TPA_Extensions.ProviderCode, //?
      airline: `#/airlines/${segment.MarketingAirline.Code}`,
      operatingAirline: segment.OperatingAirline ? `#/airlines/${segment.OperatingAirline.Code}` : null,
      flightNumber: segment.FlightNumber,
      flightMinutes: parseDuration(segment.JourneyDuration),
      cabinType: segment.BookingClassAvails.CabinType,
      isCoywolf: segment.TPA_Extensions && segment.TPA_Extensions.IsCoywolf,
      origin: `#/airports/${segment.DepartureAirport.LocationCode}`,
      destination: `#/airports/${segment.ArrivalAirport.LocationCode}`,
      departure: segment.DepartureDateTime + "-00:00",
      arrival: segment.ArrivalDateTime + "-00:00",
    })),
  })),
});

const parseDuration = str => {
  const durationRegex = /^PT((\d+)H)?(\d+)M$/;

  if ("string" != typeof str || !durationRegex.test(str))
    return 0;

  let durationMinutes = 0;

  const [
    durationMatch,
    hourPart,
    hours = "0",
    minutes = "0",
  ] = str.match(durationRegex);

  durationMinutes += parseInt(hours, 10) * 60;
  durationMinutes += parseInt(minutes, 10);

  return durationMinutes;
};

const mapTotalMinutes = itinerary => ({
  ...itinerary,
  legs: itinerary.legs.map(leg => ({
    ...leg,
    totalMinutes: leg.segments.reduce((duration, segment, index) => {
      let segmentMinutes = segment.flightMinutes;
      if (index < leg.segments.length-1) {
        const nextSegment = leg.segments[index+1];
        const connectionTime = nextSegment.departure.diff(segment.arrival, "minutes");
        segmentMinutes += connectionTime;
      }
      return duration + segmentMinutes;
    }, 0),
  })),
});

const mapLocations = airports =>
  airports.reduce((airports, location) => ({
    ...airports,
    [location.LocationCode]: {
      id: location.LocationCode,
      name: location.AirportName,
      city: location.LocationCity,
      region: location.LocationRegion,
      regionCode: location.LocationRegionCode,
      country: location.LocationCountry,
    },
  }), {});

const mapAirlines = itineraries =>
  itineraries.reduce((airlines, itinerary) => {
    itinerary.AirItinerary.OriginDestinationOptions.OriginDestinationOption.forEach(leg =>
      leg.FlightSegment.forEach(segment =>
        airlines[segment.MarketingAirline.Code] = {
          id: segment.MarketingAirline.Code,
          name: segment.MarketingAirline.CompanyShortName,
        }
      )
    );
    return airlines;
  }, {});


const searchlite = {
  id: ota.EchoToken,
  status: ota.Status,
  itineraries: ota.PricedItineraries
    .map(mapResult)
    .map(mapTotalMinutes),
  airports: mapLocations(ota.Airports),
  airlines: mapAirlines(ota.PricedItineraries),
};

console.log(JSON.stringify(searchlite, null, 2));
