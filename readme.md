# fnapi-cli

## Overview

FlightNetwork Application Programming Interface Command Line Interface (FNAPI CLI)

The main interface takes the first command line argument and attempts to call that action from the exported actions in `actions/index.js`, passing the remaining arguments (as parsed by `minimist`).

## Usage

### Installation

1. `> git clone https://bitbucket.org/justinkoreska/fnapi-cli`
1. `> npm i -g ./fnapi-cli`
1. (optional) `> export FNAPI_BASE='https://fnxml.flightnetwork.com/api'`

### Examples

+ `> fnapi search jfk.lhr.20200101,lhr.jfk.20200110 2,2,0 economy flightnetwork CAD`
+ `> fnapi review 1234567890 1`
+ `> fnapi book 1234567890 test@flightnetwork.com Roofus Summers Mr 1960 01 31 CA '123 Sesame St' Nowhere A1A1A1 4917610000000000 01 30 999`

### Search

`> fnapi search <from>.<to>.<date>[,<from>.<to>.<date>] [pax] [class] [cref] [currency]`

`--save-steps` will save step responses to `./search-<step>.json`.

### SearchLite

`> fnapi searchlite <from>.<to>.<date>[,<from>.<to>.<date>] [pax] [class] [cref] [currency]`

`--save-steps` will save the responses to `./searchlite-<step>.json`.
`--save-data` will save the data to `./searchlite.json`.
`--map-pointers` will map JSON pointers to full objects.

### Review

`> fnapi review <sid> <cid>`

### Book

_not implemented_

## TODO

+ ~~Add full search support including trip type, cabin class, and multiple pax~~
+ ~~Implement SearchLite~~
+ Implement book
+ Drink beer
